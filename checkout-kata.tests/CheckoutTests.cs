using System;
using System.Collections.Generic;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace checkout_kata.tests
{
    public class CheckoutTests
    {
        [Test]
        public void GetTotalPrice_ShouldReturnTotalPrice_ForAllScannedItems()
        {
            // arrange
            var prices = new Dictionary<string, StoreItem>
            {
                {"A", new StoreItem{ Price = 50 }},
                {"B", new StoreItem{ Price = 30 }},
                {"C", new StoreItem{ Price = 20 }},
                {"D", new StoreItem{ Price = 15 }}
            };

            var mockPricesRepo = new Mock<IItemPricesRepository>();

            mockPricesRepo
                .Setup(x => x.GetAllPrices())
                .Returns(prices);

            var checkout = new Checkout(mockPricesRepo.Object);

            // act
            checkout.Scan("A");
            checkout.Scan("B");
            checkout.Scan("C");
            checkout.Scan("D");

            // assert
            checkout.GetTotalPrice().Should().Be(50 + 30 + 20 + 15);
        }

        [Test]
        public void GetTotalPrice_ShouldThrowException_IfThereIsNoPriceForAnItem()
        {
            // arrange
            var prices = new Dictionary<string, StoreItem>();

            var mockPricesRepo = new Mock<IItemPricesRepository>();

            mockPricesRepo
                .Setup(x => x.GetAllPrices())
                .Returns(prices);

            var checkout = new Checkout(mockPricesRepo.Object);

            checkout.Scan("A");

            // act
            Action GetTotalPrice = () => checkout.GetTotalPrice();

            // assert
            GetTotalPrice.Should().Throw<Exception>().And.Message.Should().Be("item with sku A does not have prices");
        }
        [Test]
        public void Checkout_ShouldThrowArgNullException_WhenIItemPricesRepositoryIsNull()
        {
            // arrange
            IItemPricesRepository mockPricesRepo = null;

            // act
            Action checkout = () => new Checkout(mockPricesRepo);

            // assert
            checkout.Should().Throw<ArgumentNullException>().And.Message.Should().Be("Value cannot be null. (Parameter 'prices')");
        }

        [Test]
        public void Checkout_ShouldThrowArgNullException_WhenPricesAreNull()
        {
            // arrange
            Dictionary<string, StoreItem> prices = null;

            var mockPricesRepo = new Mock<IItemPricesRepository>();

            mockPricesRepo
                .Setup(x => x.GetAllPrices())
                .Returns(prices);

            // act
            Action checkout = () => new Checkout(mockPricesRepo.Object);

            // assert
            checkout.Should().Throw<ArgumentNullException>().And.Message.Should().Be("Value cannot be null. (Parameter '_prices')");
        }

        [Test]
        public void GetTotalPrice_ShouldReturnTotalPriceIncludingDiscounts_ForAllScannedItems()
        {
            // arrange
            var checkoutTestBuilder = new CheckoutTestBuilder()
                .AddCheckoutAndPrices()
                .WithItems("A,A,A") // �130
                .WithItems("A,A,A") // �260
                .WithItems("B,B") // �305
                .WithItems("B,B") // �350
                .WithItems("A,B,C,D"); // �465

            // act
            var total = checkoutTestBuilder.GetTotalPrice();

            // assert
            total.Should().Be(465);
        }

        [Test]
        public void Checkout_ShouldCallGetAllPrices_Wheninitilizing()
        {
            // arrange
            var checkoutTestBuilder = new CheckoutTestBuilder();

            // act
            checkoutTestBuilder.AddCheckoutAndPrices();

            // assert
            checkoutTestBuilder._mockRepo.Verify(x => x.GetAllPrices());
        }
    }

    internal class CheckoutTestBuilder
    {
        public Dictionary<string, StoreItem> _prices = new Dictionary<string, StoreItem>();

        public Mock<IItemPricesRepository> _mockRepo = new Mock<IItemPricesRepository>();

        public Checkout _checkout;

        public CheckoutTestBuilder AddCheckoutAndPrices()
        {
            _prices = new Dictionary<string, StoreItem>
            {
                {"A", new StoreItem{ Price = 50, MultibuyPriceAmount = 3, MultibuyPriceCost = 130 }},
                {"B", new StoreItem{ Price = 30, MultibuyPriceAmount = 2, MultibuyPriceCost = 45 }},
                {"C", new StoreItem{ Price = 20, MultibuyPriceAmount = 1, MultibuyPriceCost = 20 }},
                {"D", new StoreItem{ Price = 15, MultibuyPriceAmount = 1, MultibuyPriceCost = 15 }}
            };

            _mockRepo
             .Setup(x => x.GetAllPrices())
             .Returns(_prices);

            _checkout = new Checkout(_mockRepo.Object);

            return this;
        }

        public CheckoutTestBuilder WithItems(string items)
        {
            var itemsArray = items.Split(',');

            foreach (var item in items.Split(','))
            {
                _checkout.Scan(item);
            }

            return this;
        }

        public int GetTotalPrice() => _checkout.GetTotalPrice();
    }
}