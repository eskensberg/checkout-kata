﻿using System.Collections.Generic;

namespace checkout_kata
{
    public interface IItemPricesRepository
    {
        Dictionary<string, StoreItem> GetAllPrices();
    }
}