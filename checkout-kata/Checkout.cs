﻿using System;
using System.Collections.Generic;

namespace checkout_kata
{
    public class Checkout : ICheckout
    {
        private readonly Dictionary<string, int> _store = new Dictionary<string, int>();
        private readonly Dictionary<string, StoreItem> _prices;

        public Checkout(IItemPricesRepository prices)
        {
            if (prices == null) throw new ArgumentNullException(nameof(prices));

            _prices = prices.GetAllPrices() ?? throw new ArgumentNullException(nameof(_prices));
        }

        public void Scan(string item)
        {
            if (_store.TryGetValue(item, out var amount))
            {
                _store[item] = amount + 1;
            }
            else
            {
                _store[item] = 1;
            }
        }

        public int GetTotalPrice()
        {
            var total = 0;

            foreach (var item in _store)
            {
                total += AddItemCost(item);
            }

            return total;
        }

        private int AddItemCost(KeyValuePair<string, int> item)
        {
            if (_prices.TryGetValue(item.Key, out var storeItemPrice))
            {
                if (storeItemPrice.MultibuyPriceAmount > 0)
                {
                    var multibuyPrice = item.Value / storeItemPrice.MultibuyPriceAmount * storeItemPrice.MultibuyPriceCost;

                    var modulus = (decimal)item.Value % (decimal)storeItemPrice.MultibuyPriceAmount * storeItemPrice.Price;

                    var total = multibuyPrice + (int)modulus;
                    return total;
                }
                else
                {
                    return storeItemPrice.Price;
                }
            }
            else
            {
                throw new Exception($"item with sku {item.Key} does not have prices");
            }
        }
    }
}
