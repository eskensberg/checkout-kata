﻿namespace checkout_kata
{
    public class StoreItem
    {
        public int Price { get; set; }
        public int MultibuyPriceAmount { get; set; }
        public int MultibuyPriceCost { get; set; }
    }
}
